package org.eyo.damas.controllers;

import org.eyo.damas.models.Coordinate;
import org.eyo.damas.models.Game;
import org.eyo.damas.models.Piece;
import org.eyo.damas.models.State;

public abstract class InteractorController extends Controller {

	protected InteractorController(Game game, State state) {
		super(game, state);
	}

	public Piece getPiece(Coordinate coordinate) {
		return this.game.getPiece(coordinate);
	}

	abstract public void accept(InteractorControllersVisitor controllersVisitor);

}
