package org.eyo.damas.controllers;

import org.eyo.damas.models.Game;
import org.eyo.damas.models.State;

class CancelController extends Controller {

    protected CancelController(Game game, State state) {
        super(game, state);
    }

    public void cancel() {
		this.game.cancel();
		this.state.next();
	}

}