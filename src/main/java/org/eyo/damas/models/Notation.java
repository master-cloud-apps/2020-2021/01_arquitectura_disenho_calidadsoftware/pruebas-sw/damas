package org.eyo.damas.models;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class Notation {

    private Map<String, Integer> mathColumns;
    private Map<String, Integer> mathRows;

    public Notation() {
        this.mathColumns = new HashMap<>();
        this.mathRows = new HashMap<>();
        this.mathRows.put("8", 1);
        this.mathRows.put("7", 2);
        this.mathRows.put("6", 3);
        this.mathRows.put("5", 4);
        this.mathRows.put("4", 5);
        this.mathRows.put("3", 6);
        this.mathRows.put("2", 7);
        this.mathRows.put("1", 8);
        this.mathColumns.put("a", 1);
        this.mathColumns.put("b", 2);
        this.mathColumns.put("c", 3);
        this.mathColumns.put("d", 4);
        this.mathColumns.put("e", 5);
        this.mathColumns.put("f", 6);
        this.mathColumns.put("g", 7);
        this.mathColumns.put("h", 8);
    }

    public NotationType getNotationType(String square) {
        if (Pattern.matches("[a-h][1-8]", square)){
            return NotationType.ALGEBRAIC;
        }
        if (Pattern.matches("[1-8]{2}", square)){
            return NotationType.MATHEMATICAL;
        }
        return NotationType.DESCRIPTIVE;
    }

    public String getMathPosition(String algebraicSquare) {
        assert algebraicSquare.split("").length == 2;
        assert NotationType.ALGEBRAIC == this.getNotationType(algebraicSquare);

        String row = algebraicSquare.split("")[1];
        String sColumn = algebraicSquare.split("")[0];
        return Integer.toString(this.mathRows.get(row)) + this.mathColumns.get(sColumn);
    }
}
