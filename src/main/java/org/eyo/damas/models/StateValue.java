package org.eyo.damas.models;

public enum StateValue {
	INITIAL, 
	IN_GAME, 
	FINAL, 
	EXIT;
}
