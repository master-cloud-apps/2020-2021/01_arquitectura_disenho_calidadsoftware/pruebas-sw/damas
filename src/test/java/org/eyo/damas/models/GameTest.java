package org.eyo.damas.models;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class GameTest {

    private Game game;

    @Before
    public void setUp(){
        this.game = new Game();
    }

    @Test
    public void should_not_be_null(){
        assertNotNull(this.game);
    }

    @Test
    public void should_reset_the_board(){
        this.game.reset();

        assertNull(this.game.getColor(Coordinate.getInstance("h1")));
        assertNull(this.game.getColor(Coordinate.getInstance("f1")));
        assertNull(this.game.getColor(Coordinate.getInstance("g2")));
        assertEquals(Color.WHITE, this.game.getColor(Coordinate.getInstance("g1")));
        assertEquals(Color.WHITE, this.game.getColor(Coordinate.getInstance("h2")));
        assertEquals(Color.WHITE, this.game.getColor(Coordinate.getInstance("g3")));
        assertEquals(Color.WHITE, this.game.getColor(Coordinate.getInstance("a1")));

        assertNull(this.game.getColor(Coordinate.getInstance("g8")));
        assertNull(this.game.getColor(Coordinate.getInstance("f7")));
        assertNull(this.game.getColor(Coordinate.getInstance("e6")));
        assertEquals(Color.BLACK, this.game.getColor(Coordinate.getInstance("h8")));
        assertEquals(Color.BLACK, this.game.getColor(Coordinate.getInstance("g7")));
        assertEquals(Color.BLACK, this.game.getColor(Coordinate.getInstance("a7")));
        assertEquals(Color.BLACK, this.game.getColor(Coordinate.getInstance("b8")));
    }

    @Test
    public void should_cancel_the_game(){
        this.game.reset();

        this.game.cancel();

        assertNull(this.game.getColor(Coordinate.getInstance("g1")));
        assertEquals(Color.BLACK, this.game.getTurnColor());
    }
}
