package org.eyo.damas.models;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CorrectMovesPawnGameTest extends BoardTest{

    @Test
    public void should_eat_one_black_pawn(){
        this.setGame(Color.WHITE, "f2w", "e3b");
        this.setExpectedGame(Color.BLACK, "d4w");
        this.game.move(Coordinate.getInstance("f2"), Coordinate.getInstance( "d4"));

        assertEquals(this.expectedGame, this.game);
        assertTrue(this.game.isBlocked());
    }

    @Test
    public void should_eat_two_black_pawns_not_in_diagonal(){
        this.setGame(Color.WHITE, "f2w", "e3b", "e5b");
        this.setExpectedGame(Color.BLACK, "f6w");
        this.game.move(
                Coordinate.getInstance("f2"),
                Coordinate.getInstance("d4"),
                Coordinate.getInstance( "f6"));

        assertEquals(this.expectedGame, this.game);
        assertTrue(this.game.isBlocked());
    }

    @Test
    public void should_eat_two_black_pawns_in_diagonal(){
        this.setGame(Color.WHITE, "f2w", "e3b", "c5b");
        this.setExpectedGame(Color.BLACK, "b6w");
        this.game.move(
                Coordinate.getInstance("f2"),
                Coordinate.getInstance("d4"),
                Coordinate.getInstance( "b6")
        );

        assertEquals(this.expectedGame, this.game);
        assertTrue(this.game.isBlocked());
    }

    @Test
    public void should_move_one_pawn(){
        this.setGame(Color.WHITE, "f2w", "f6b");
        this.setExpectedGame(Color.BLACK, "e3w", "f6b");
        this.game.move(Coordinate.getInstance("f2"), Coordinate.getInstance( "e3"));

        assertEquals(this.expectedGame, this.game);
        assertFalse(this.game.isBlocked());
    }

    @Test
    public void should_check_strings(){
        this.setGame(Color.WHITE, "f2w", "f6b");
        this.setExpectedGame(Color.BLACK, "e3w", "f6b");
        this.game.move(Coordinate.getInstance("f2"), Coordinate.getInstance( "e3"));

        assertEquals(this.expectedGame.toString(), this.game.toString());
    }
}
