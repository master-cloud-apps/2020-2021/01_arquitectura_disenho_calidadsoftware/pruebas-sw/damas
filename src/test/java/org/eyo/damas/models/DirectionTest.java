package org.eyo.damas.models;

import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;

public class DirectionTest {

    private Direction direction;

    @Before
    public void setUp(){
        this.direction = Direction.NE;
    }

    @Test
    public void should_get_on_direction_false(){
        assertFalse(this.direction.isOnDirection(Coordinate.getInstance("a8")));
    }
}
