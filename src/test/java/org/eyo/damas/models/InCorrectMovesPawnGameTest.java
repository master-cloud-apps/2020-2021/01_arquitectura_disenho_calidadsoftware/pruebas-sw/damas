package org.eyo.damas.models;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class InCorrectMovesPawnGameTest extends BoardTest {

    @Test
    public void should_not_eat_one_black_pawn_and_move_more_than_one() {
        this.setGame(Color.WHITE, "f2w", "e3b");
        Error error = this.game.move(Coordinate.getInstance("f2"), Coordinate.getInstance("c5"));

        assertEquals(Error.TOO_MUCH_ADVANCED, error);
    }

    @Test
    public void should_not_move_one_empty_square() {
        this.setGame(Color.WHITE, "f2w", "e3b");
        Error error = this.game.move(Coordinate.getInstance("g1"), Coordinate.getInstance("c5"));

        assertEquals(Error.EMPTY_ORIGIN, error);
    }

    @Test
    public void should_not_move_to_position_where_opponent_piece_is() {
        this.setGame(Color.WHITE, "f2w", "e3b");
        Error error = this.game.move(Coordinate.getInstance("f2"), Coordinate.getInstance("e3"));

        assertEquals(Error.NOT_EMPTY_TARGET, error);
    }

    @Test
    public void should_not_move_opponent_piece() {
        this.setGame(Color.WHITE, "f2w", "e3b");
        Error error = this.game.move(Coordinate.getInstance("e3"), Coordinate.getInstance("g1"));

        assertEquals(Error.OPPOSITE_PIECE, error);
    }

    @Test
    public void should_not_move_not_diagonal() {
        this.setGame(Color.WHITE, "f2w", "e3b");
        Error error = this.game.move(Coordinate.getInstance("f2"), Coordinate.getInstance("f6"));

        assertEquals(Error.NOT_DIAGONAL, error);
    }

    @Test
    public void should_not_move_not_advanced() {
        this.setGame(Color.WHITE, "f2w", "e3b");
        Error error = this.game.move(Coordinate.getInstance("f2"), Coordinate.getInstance("g1"));

        assertEquals(Error.NOT_ADVANCED, error);
    }

    @Test
    public void should_not_eat_same_color_piece() {
        this.setGame(Color.WHITE, "f2w", "e3w");
        Error error = this.game.move(Coordinate.getInstance("f2"), Coordinate.getInstance("d4"));

        assertEquals(Error.COLLEAGUE_EATING, error);
    }

    @Test
    public void should_not_jump_too_much() {
        this.setGame(Color.WHITE, "f2w", "e3b", "c5b");
        Error error = this.game.move(Coordinate.getInstance("f2"), Coordinate.getInstance("d4"),
                Coordinate.getInstance("e5"));

        assertEquals(Error.TOO_MUCH_JUMPS, error);
    }

    @Test
    public void should_not_advance_too_much() {
        this.setGame(Color.WHITE, "f2w", "b8b");
        Error error = this.game.move(Coordinate.getInstance("f2"), Coordinate.getInstance("a7"));

        assertEquals(Error.TOO_MUCH_ADVANCED, error);
    }

    @Test
    public void should_not_eat_and_advance_too_much() {
        this.setGame(Color.WHITE, new String[]{"f2w", "e3b", "c5B"});
        this.setExpectedGame(Color.WHITE, new String[]{"f2w", "e3b", "c5B"});
        Error error = this.game.move(
                Coordinate.getInstance("f2"),
                Coordinate.getInstance("d4"),
                Coordinate.getInstance("b6"),
                Coordinate.getInstance("a7"));

        assertEquals(Error.TOO_MUCH_JUMPS, error);
        assertEquals(this.expectedGame, this.game);
    }

    @Test
    public void should_not_eat_and_make_queen_and_advance_too_much() {
        this.setGame(Color.WHITE, new String[]{"f6w", "e7b"});
        this.setExpectedGame(Color.WHITE, new String[]{"f6w", "e7b"});
        Error error = this.game.move(
                Coordinate.getInstance("f6"),
                Coordinate.getInstance("d8"),
                Coordinate.getInstance("e7"));

        assertEquals(Error.TOO_MUCH_JUMPS, error);
        assertEquals(this.expectedGame, this.game);
    }

}
