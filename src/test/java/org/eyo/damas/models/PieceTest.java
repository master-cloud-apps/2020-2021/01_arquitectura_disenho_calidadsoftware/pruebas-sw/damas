package org.eyo.damas.models;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PieceTest {

    @Test
    public void should_get_string_from_pawn(){
        Piece whitePawn = new Pawn(Color.WHITE);
        Piece blackQueen = new Draught(Color.BLACK);

        assertEquals("w", whitePawn.toString());
        assertEquals("B", blackQueen.toString());
    }
}
