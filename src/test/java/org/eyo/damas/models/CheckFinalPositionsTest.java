package org.eyo.damas.models;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CheckFinalPositionsTest extends BoardTest{

    @Test
    public void should_win_black_white_blocked(){
        this.setGame(Color.BLACK, "a1w", "a3b", "c3b");

        this.game.move(Coordinate.getInstance("a3"), Coordinate.getInstance("b2"));

        assertTrue(this.game.isBlocked());
    }

    @Test
    public void should_win_black_two_whites_blocked(){
        this.setGame(Color.BLACK, "a1w", "a7w", "b8b", "a3b", "c3b");

        this.game.move(Coordinate.getInstance("a3"), Coordinate.getInstance("b2"));

        assertTrue(this.game.isBlocked());
    }

    @Test
    public void should_not_black_two_whites_blocked_one_not(){
        this.setGame(Color.BLACK, "a1w", "a7w", "g1w", "b8b", "a3b", "c3b");

        this.game.move(Coordinate.getInstance("a3"), Coordinate.getInstance("b2"));

        assertFalse(this.game.isBlocked());
    }


}
