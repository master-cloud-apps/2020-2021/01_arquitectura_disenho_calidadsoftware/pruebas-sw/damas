package org.eyo.damas.views;

import org.eyo.damas.controllers.*;
import org.eyo.damas.models.Game;
import org.eyo.damas.models.State;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class ViewTest {

    @InjectMocks
    private View view;
    @Mock
    private PlayView playView;
    @Mock
    private ResumeView resumeView;
    private Logic logic;
    private Game game;
    private State state;
    private PlayController playController;
    private ResumeController resumeController;

    @BeforeEach
    public void setUp(){
        this.logic = new Logic();
        this.game = new Game();
        this.state = new State();
        this.playController = new PlayController(this.game, this.state);
        this.resumeController = new ResumeController(this.game, this.state);
    }

    @Test
    public void should_interact_from_start_to_play(){
        assertEquals(StartController.class, this.logic.getController().getClass());

        this.view.interact(this.logic.getController());

        assertEquals(PlayController.class, this.logic.getController().getClass());
    }

    @Test
    public void should_interact_with_play_controller(){
        this.view.interact(this.playController);

        verify(this.playView).interact(this.playController);
    }

    @Test
    public void should_interact_with_resume_controller(){
        this.view.interact(this.resumeController);

        verify(this.resumeView).interact(this.resumeController);
    }
}
