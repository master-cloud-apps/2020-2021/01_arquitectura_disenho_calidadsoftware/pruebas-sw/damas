package org.eyo.damas.views;

import org.eyo.damas.controllers.PlayController;
import org.eyo.damas.models.*;
import org.eyo.damas.utils.Console;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class PlayViewTest {

    private static final String USER_CANCEL = "-1";

    @InjectMocks
    private PlayView playView;

    @Mock
    private Console console;

    @Mock
    private PlayController playControllerMock;

    private PlayController playController;
    private Game game;
    private State state;

    @BeforeEach
    public void setUp() {
        this.game = new Game();
        this.state = new State();
        this.playController = new PlayController(this.game, this.state);
        this.state.next();
    }

    @Test
    public void should_generate_instance_class() {
        assertNotNull(this.playView);
    }

    @Test
    public void should_interact_end_game() {
        setFirstMove(console, USER_CANCEL);

        this.playView.interact(this.playController);

        assertEquals(StateValue.FINAL, this.state.getValueState());
    }

    @Test
    public void should_ask_for_new_string_when_error() {
        when(this.console.readString(anyString())).thenAnswer(new Answer<String>() {
            private int iteration = 0;
            private String[] userInteractions = {"Bad Move Format", USER_CANCEL};

            @Override
            public String answer(InvocationOnMock invocationOnMock) {
                iteration++;
                return userInteractions[iteration - 1];
            }
        });

        this.playView.interact(this.playController);

        assertEquals(StateValue.FINAL, this.state.getValueState());
    }

    @Test
    public void should_play_first_move_and_change_turn() {
        setFirstMove(console, "g3.h4");

        this.playView.interact(this.playController);

        assertEquals(StateValue.IN_GAME, this.state.getValueState());
        assertEquals(Color.BLACK, this.game.getTurnColor());
        assertEquals(Color.BLACK, this.playController.getColor());
    }

    @Test
    public void should_play_one_move_and_win_the_game() {
        setFirstMove(console, "g3.h4");
        when(this.playControllerMock.isBlocked()).thenReturn(true);
        when(this.playControllerMock.getColor()).thenReturn(Color.WHITE);
        when(this.playControllerMock.move(
                Coordinate.getInstance("g3"),
                Coordinate.getInstance("h4")))
                .thenAnswer(invocationOnMock -> {
                    state.next();
                    return null;
                });

        this.playView.interact(this.playControllerMock);

        assertEquals(StateValue.FINAL, this.state.getValueState());
    }

    private static void setFirstMove(Console console, String move){
        when(console.readString(anyString())).thenReturn(move);
    }
}
