package org.eyo.damas.dtos;

import org.eyo.damas.models.Coordinate;
import org.eyo.damas.models.Piece;

public class PieceInBoard {

    private Coordinate coordinate;
    private Piece kind;

    public PieceInBoard(Coordinate coordinate, Piece kind) {
        this.coordinate = coordinate;
        this.kind = kind;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public Piece getKind() {
        return kind;
    }
}
