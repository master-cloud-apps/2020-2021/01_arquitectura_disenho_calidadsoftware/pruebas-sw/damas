package org.eyo.damas.dtos;

import org.eyo.damas.models.Piece;

import java.util.List;

public class InitialPosition {

    private List<PieceInBoard> pieces;

    public InitialPosition(List<PieceInBoard> pieces) {
        this.pieces = pieces;
    }

    public List<PieceInBoard> getPieces() {
        return pieces;
    }
}
