package org.eyo.damas.utils;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class YesNoDialogTest {

    @InjectMocks
    private YesNoDialog yesNoDialog;

    @Mock
    private Console console;

    @Test
    public void should_not_be_null(){
        assertNotNull(this.yesNoDialog);
    }

    @Test
    public void should_read_ok_from_user_when_put_y(){
        when(this.console.readChar(anyString())).thenReturn('y');

        assertTrue(this.yesNoDialog.read("Do you want to continue (y/n): "));
    }

    @Test
    public void should_read_ko_from_user_when_put_n(){
        when(this.console.readChar(anyString())).thenReturn('n');

        assertFalse(this.yesNoDialog.read("Do you want to continue (y/n): "));
    }

    @Test
    public void should_read_ko_from_user_when_put_n_third_try(){
        when(this.console.readChar(anyString())).thenAnswer(getCharAnswers(new char[]{'b', 'b', 'n'}));

        assertFalse(this.yesNoDialog.read("Do you want to continue (y/n): "));
    }

    @Test
    public void should_read_ok_from_user_when_put_y_third_try(){
        when(this.console.readChar(anyString())).thenAnswer(getCharAnswers(new char[]{'b', 'b', 'y'}));

        assertTrue(this.yesNoDialog.read("Do you want to continue (y/n): "));
    }

    private Answer getCharAnswers(char[] answers){
        return new Answer<>() {
            private int times = 0;

            @Override
            public Object answer(InvocationOnMock invocationOnMock) {
                return answers[times++];
            }
        };
    }
}
